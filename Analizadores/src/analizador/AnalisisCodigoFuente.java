package analizador;

import java.util.*;
import analizador.expresion.ExpresionRegular;
import ejecucion.Sentencia;
import ejecucion.Variable;
import excepciones.*;
import javax.swing.DefaultListModel;
import postFijo.PostFijo;
import util.Busquedas;
import util.pila.Pila;

public class AnalisisCodigoFuente {
    private int numLinea;
    boolean fuenteAceptada;
    private ArrayList<Sentencia> fuente;
    private ArrayList<Variable> listaVariables;
    int cont ;
    private DefaultListModel<String> model = new DefaultListModel<String>();
    private Stack<ErrorLinea> errores;
    public AnalisisCodigoFuente (ArrayList<Sentencia> fuente) {
            this.fuente = fuente;
            numLinea = 1;
            fuenteAceptada = true;
            model = new DefaultListModel<String>();
            errores = new Stack<>();
    }

    public boolean isFuenteAceptada() {
        return fuenteAceptada;
    }
	
            
    public int getNumLinea() {
            return numLinea;
    }

    public ArrayList<Sentencia> getFuente() {
            return fuente;
    }

    public ArrayList<Variable> getListaVariables() {
            return listaVariables;
    }
    
	public int getCont() {
		return cont;
	}

	public DefaultListModel<String> getModel() {
		return model;
	}

	public void compilar (){
		
		errores.clear();
        fuenteAceptada = true;
        Iterator<Sentencia> miIterador = fuente.iterator();
        model.clear();
        cont = 0;
        listaVariables = new ArrayList<Variable>();
        numLinea = 1;
        Sentencia linea;
        
        while (miIterador.hasNext()){
            linea = miIterador.next();
            if(ExpresionRegular.declararacioVariables(linea.getLinea())){
                linea.setTipo("DECLARACION_VARIABLE");
                asignacionVariables(linea);
            } else if (ExpresionRegular.esOperacion(linea.getLinea())){
                linea.setTipo("OPERACION");
                operar(linea);
            } else if (ExpresionRegular.esAsignacionDeCadena(linea.getLinea())){
                linea.setTipo("ASIGNACION_CADENA_CARACTER");
                asignarCadenas(linea);
            } else if (ExpresionRegular.esSalida(linea.getLinea())){
                linea.setTipo("ESCRIBIR");
                escribir(linea);
            } else if (ExpresionRegular.esEntrada(linea.getLinea())){
                linea.setTipo("LEER");
            }

            numLinea++;
        }
    }

    public void escribir (Sentencia linea){
        String tokenActual = "";
        String imprimir = "";
        Object valor;
        Scanner scan;
        scan = new Scanner(linea.getLinea());
        tokenActual = "";
        imprimir = "";
        while (tokenActual != "$") {
            tokenActual = scan.sgteToken();

            if (ExpresionRegular.esConstanteCaracter(tokenActual)) {
                if(tokenActual.startsWith("\"") && tokenActual.endsWith("\"")){
                    imprimir += tokenActual.substring(1, tokenActual.length()-1);
                }
            } else if (ExpresionRegular.esID(tokenActual)){
                if (Busquedas.busqueda(listaVariables, tokenActual)!= -1) {
                    valor = listaVariables.get(Busquedas.busqueda(listaVariables, tokenActual)).getValor();
                    if(valor != null){
                        imprimir += valor.toString();
                    } else {
                       imprimir += "nulo";
                    }
                } else {
                    fuenteAceptada = false;
                    errores.push(new ErrorLinea(ErrorLinea.VARIABLE_NO_DECLARADA,
							linea.getNumLinea()));
                }
            }
        }
        model.add(cont,imprimir);
        cont ++;
       	
	}
	
	
	private void asignacionVariables(Sentencia linea){
		Scanner scan;
		String tokenActual,id = "",tipo = "",valor = "";
		scan = new Scanner(linea.getLinea());
		tokenActual = "";
		
		while (tokenActual != "$") {
			tokenActual = scan.sgteToken();
			
			if (ExpresionRegular.esTipo(tokenActual)) {
				tipo = tokenActual;
			} else if (ExpresionRegular.esID(tokenActual)) {
				id = tokenActual;
				if (Busquedas.busqueda(listaVariables, id) != -1) {
                                                                fuenteAceptada = false;
                    errores.push(new ErrorLinea(ErrorLinea.DOBLE_ASIGNACION,linea.getNumLinea()));
				} 
				valor = null;
			} else if (ExpresionRegular.esConstante(tokenActual) || ExpresionRegular.esID(tokenActual) || 
						tokenActual.equals("-")){
				
				if (tipo.equals("cadena")){
					if(tokenActual.startsWith("\"") && tokenActual.endsWith("\"")){
						valor = tokenActual.substring(1, tokenActual.length()-1);
					} else {
                        fuenteAceptada = false;
                        errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN +" (*->cadena)",linea.getNumLinea()));
					}
				} else if (tipo.equals("caracter")){
					if(tokenActual.startsWith("\'") && tokenActual.endsWith("\'")) {
						valor = "" + tokenActual.charAt(1);
					} else {
                        fuenteAceptada = false;
                        errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN +" (*->caracter)",linea.getNumLinea()));
					}
				} else if (tipo.equals("entero")){
					if (tokenActual.equals("-")) {
						valor = tokenActual;
						tokenActual = scan.sgteToken();
					}
					if (ExpresionRegular.esEntero(tokenActual)){
						valor += tokenActual;
					} else {
                        fuenteAceptada = false;
                        errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN + " (*->entero)",linea.getNumLinea()));
					}
				} else if (tipo.equals("real")){
					if (tokenActual.equals("-")) {
						valor = tokenActual;
						tokenActual = scan.sgteToken();
					} 
					if(ExpresionRegular.esReal(tokenActual)){
						valor = tokenActual;
					} else {
                        fuenteAceptada = false;
                        errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN + " (*->real)",linea.getNumLinea()));
					}
				} else if(tipo.equals("boleano")) {
					if(ExpresionRegular.esBoleano(tokenActual)){
						valor = tokenActual;
					} else {
                        fuenteAceptada = false;
                        errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN + " (*->boleano)",linea.getNumLinea()));
					}
				} 
			}	
			if (valor == null) {
				valor = valorPorDefecto(tipo);
			}
			if (tokenActual.equals(",")||tokenActual.equals("$")) {
				listaVariables.add(crearNuevaVariable(id, tipo, valor));
			}
		}
	}
        
        
	private void asignarCadenas(Sentencia linea){
		Scanner scan;
		String tokenActual,id = "",tipo = "",valor = "";
		Variable varAsignar = null;
		scan = new Scanner(linea.getLinea());
		tokenActual = "";
		
		while (tokenActual != "$") {
			tokenActual = scan.sgteToken();
			if (ExpresionRegular.esID(tokenActual)) {
				id = tokenActual;
				if (Busquedas.busqueda(listaVariables, id) == -1) {
                   fuenteAceptada = false;
                   errores.push(new ErrorLinea(ErrorLinea.VARIABLE_NO_DECLARADA,linea.getNumLinea()));
				}  else {
					varAsignar = listaVariables.get(Busquedas.busqueda(listaVariables, id));
					tipo = varAsignar.getTipo();
				}
			} else if (ExpresionRegular.esConstanteCaracter(tokenActual)){
				if (tipo.equals("cadena")){
					if(tokenActual.startsWith("\"") && tokenActual.endsWith("\"")){
						valor = tokenActual.substring(1, tokenActual.length()-1);
					} else {
                        fuenteAceptada = false;
                        errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN +" (*->cadena)",linea.getNumLinea()));
					}
				} else if (tipo.equals("caracter")){
					if(tokenActual.startsWith("\'") && tokenActual.endsWith("\'")) {
						valor = "" + tokenActual.charAt(1);
					} else {
                        fuenteAceptada = false;
                        errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN +" (*->caracter)",linea.getNumLinea()));
					}
				}
			}
			
			if (tokenActual.equals("$")) {
				varAsignar.setValor(valor);
			}
		}
		
	}
	private void operar(Sentencia linea){
		Scanner scan;
		String tokenActual, inFijo = "",postFijo;
		Variable varAsignar;
		int resInt,posVarAsignar;
		double resFloat;
		
		scan = new Scanner(linea.getLinea());
		
		tokenActual = scan.sgteToken();
		posVarAsignar = Busquedas.busqueda(listaVariables, tokenActual);
		
		if(posVarAsignar != -1){
			
			varAsignar = listaVariables.get(posVarAsignar);
			tokenActual = scan.sgteToken();
			
			if (tokenActual.equals("=")) {
				
				tokenActual = scan.sgteToken();
				
				do {
					inFijo += tokenActual;
					tokenActual = scan.sgteToken();
				} while (!tokenActual.equals("$"));
				
				postFijo = PostFijo.convertirPostFijo(inFijo);
				
				if(varAsignar.getTipo().equals("entero")){
					resInt = PostFijo.obtenerResultadoInt(listaVariables,errores,linea, postFijo);
					varAsignar.setValor(resInt);
				} else if (varAsignar.getTipo().equals("real")) {
					resFloat = PostFijo.obtenerResultadoDouble(listaVariables,errores,linea, postFijo);
					varAsignar.setValor(resFloat);
				}
			}
			
		} else {
			fuenteAceptada = false;
			errores.push(new ErrorLinea(ErrorLinea.VARIABLE_NO_DECLARADA,linea.getNumLinea()));
		}		
	}
	
	
		
	
	public static String valorPorDefecto(String tipoVariable){
		switch (tipoVariable) {
		case  "entero" :
			return "0";
		case "real" :
			return "0.0";
		case "cadena":
			return null;
		case "caracter" :
			return "";
		case "boleano" : 
			return "falso";
		default: 
			return null;
		}
	 }
	
	private static Variable crearNuevaVariable (String id,String tipo, String valor) {
		if (valor != null) {
			switch (tipo) {
			case  "entero" :
				return new Variable(id,tipo,Integer.parseInt(valor));
			case "real" :
				return new Variable(id,tipo,Double.parseDouble(valor));
			case "cadena":
				return new Variable(id,tipo,valor);
			case "caracter" :
				return new Variable(id,tipo,valor.charAt(0));
			case "boleano" : 
				if (valor.equals("cierto")){
					return new Variable(id,tipo,true);
				}else if (valor.equals("falso")) {
					return new Variable(id,tipo,false);
				}
				
			default: 
				return null;
			}
		} else {
			return new Variable(id,tipo,valor);
		}
	}
	
	public static <T> void imprimirPilaDeErrores (Stack<T> errores){	
		for (Iterator<T> iterator = errores.iterator(); iterator.hasNext();) {
			T t = iterator.next();
			System.out.println(t);
		}
	}

	public Stack<ErrorLinea> getErrores() {
		return errores;
	}
	
	
}
