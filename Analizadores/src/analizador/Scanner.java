package analizador;

public class Scanner {
	
	private int j;
	private String cadena;
	private int numToken;
	
	public Scanner(String cadena){
		this.cadena = cadena;
		j = 0;
		numToken = 0;
	}
	
	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public String getCandidata() {
		return cadena;
	}

	public void setCandidata(String candidata) {
		this.cadena = candidata;
	}
	
	public int getNumToken() {
		return numToken;
	}

	public void setNumToken(int numToken) {
		this.numToken = numToken;
	}

	public String sgteToken(){
		String token = "";
		if(j < cadena.length() && cadena.charAt(j) == ' '){
			j++;
		}
		if(j < cadena.length()){
			if(Character.isLetter(cadena.charAt(j))){
				while(j < cadena.length() && (Character.isLetter(cadena.charAt(j))|| cadena.charAt(j) == '_' || Character.isDigit(cadena.charAt(j)))){
					token = token + cadena.charAt(j);
					j++;
				}
			}else if(Character.isDigit(cadena.charAt(j))){
				while( j < cadena.length() && Character.isDigit(cadena.charAt(j)) ){
					token = token + cadena.charAt(j);
					j++;
				}
				
				if(j < cadena.length() && cadena.charAt(j) == '.'){
					token = token + cadena.charAt(j);
					j++;
					while( j < cadena.length() && Character.isDigit(cadena.charAt(j)) ){
						token = token + cadena.charAt(j);
						j++;
					}
				}
			}else if(cadena.charAt(j)== '<' || cadena.charAt(j) == '>'){
				if(cadena.charAt(j+1) == '='){
					token = token + cadena.charAt(j) + cadena.charAt(j+1);
					j += 2;
				}else{
					token = token + cadena.charAt(j);
					j++;
				}
			}else if(cadena.charAt(j) == '+' || cadena.charAt(j) == '-'){
				if(cadena.charAt(j+1) == '+' || cadena.charAt(j+1) == '-'){
					token = token + cadena.charAt(j) + cadena.charAt(j+1);
					j += 2;
				} else {
					
					token = token + cadena.charAt(j);
					j++;
				}
			}else if(cadena.charAt(j) == '(' || cadena.charAt(j) == ')' || cadena.charAt(j) == ';' || 
				cadena.charAt(j) == '=' || cadena.charAt(j) =='{'|| cadena.charAt(j) =='}' ||
				cadena.charAt(j) == ','){
				token = token + cadena.charAt(j);
				j ++;
			}else if (cadena.charAt(j) == '"'){
				token = token + cadena.charAt(j);
				j++;
				while(j < cadena.length() && (Character.isDefined(cadena.charAt(j)))){
					token = token + cadena.charAt(j);
					if( cadena.charAt(j) == '"'){
						j++;
						break;
						
					} else {
						j++;
					}
				}
				
			} else if (cadena.charAt(j) == '\''){
				token = token + cadena.charAt(j);
				j++;
				if (Character.isDefined(cadena.charAt(j))){
					token = token + cadena.charAt(j);
					j++;
					if( cadena.charAt(j) == '\''){
						token = token + cadena.charAt(j);
						j++;
					}
				}
			} else if (cadena.charAt(j) == '/' || cadena.charAt(j) == '*' || cadena.charAt(j) == '^'){
				token = token + cadena.charAt(j);
				j++;
			}
			
		}else if(j == cadena.length()){
			token = "$";
			j++;
		}
		
		numToken++;
		return token;
	}

	
}
