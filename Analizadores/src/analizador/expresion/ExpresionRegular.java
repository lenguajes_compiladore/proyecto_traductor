package analizador.expresion;
import java.util.ArrayList;
import java.util.regex.*;
import analizador.*;
import util.pila.Pila;

public class ExpresionRegular {

	public static boolean declararacioVariables(String declaracion){
		int inicio = 0, fin = 5,error = -1, actual = inicio;
		Scanner scan = new Scanner(declaracion);
		String token = "";
		while(actual != error &&  token != "$"){
			token = scan.sgteToken();
			switch(actual){
			case 0:
				if (esTipo(token)){
					actual = 1;
				} else {
					actual = error;
				}
				break;
			case 1:
				if (esID(token)){
					actual = 2;
				} else {
					actual = error;
				}
				break;
			case 2:
				if (token.compareTo("$") == 0){
					actual = fin;
				} else if (token.compareTo(",") == 0){
					actual = 1;
				} else if (token.compareTo("=") == 0){
					actual = 3;
				} else {
					actual = error;
				}
				break;
			case 3:
				if (esID(token) || esConstante(token)){
					actual = 4;
				} else if (token.equals("-")){
					actual = 6;
				} else {
					actual = error;
				}
				break;
			case 4:
				if (token.compareTo("$") == 0){
					actual = fin;
				} else if (token.compareTo(",") == 0){
					actual = 1;
				} else {
					actual = error;
				}
				break;
			case 5 :
				if (esReal(token)){
					actual = 6;
				} else {
					 actual = error;
				}
				break;
			case 6: 
				if (token.compareTo("$") == 0){
					actual = fin;
				} else if (token.compareTo(",") == 0){
					actual = 1;
				} else {
					actual = error;
				}
				break;
			}
			
		
		}
		
		if(actual == fin){
			return true;
		} else {
			return false;
		}
		
	}
	
	
	
	public static boolean esTipo(String cadena){
		return cadena.equals("entero") || cadena.equals("real")||
				cadena.equals("caracter")|| cadena.equals("boleano") || 
				cadena.equals("cadena");
	}
	
        public static boolean esTipoFuncion(String cadena){
		return cadena.equals("entero") || cadena.equals("real")||
				cadena.equals("caracter")|| cadena.equals("boleano") || 
				cadena.equals("vacio") || cadena.equals("cadena");
	}
	
	public static boolean esID (String tipo){
		return expresionRegular(tipo, "[a-z][a-zA-Z_1-9]*") && !esPalabraReservada(tipo);
	}
	
	private static boolean esPalabraReservada(String tipo) {
		
		return esBoleano(tipo) || tipo.equals("nulo") || tipo.equals("leer") 
				|| tipo.equals("escribir") || esTipoFuncion(tipo);
	}

	public static boolean esConstante(String tipo){
		return expresionRegular(tipo, "((-)?[0-9]+(.[0-9]+)?|\"(\\p{Print})+\"|\'(\\p{Print})\')") 
				|| tipo.equals("nulo") || esBoleano(tipo);
	}
	
	public static boolean esReal(String tipo) {
		return expresionRegular(tipo, "(-)?[0-9]+(.[0-9]+)?");
	}
	
	public static boolean esEntero(String tipo) {
		return expresionRegular(tipo,"(-)?[0-9]+");
	}

	public static boolean esBoleano(String tipo){
		return tipo.equals("cierto") || tipo.equals("falso");
	}
	
	public static boolean expresionRegular(String tipo,String expre){
		Pattern patron = Pattern.compile(expre);
		Matcher comparador = patron.matcher(tipo);
		return comparador.matches();
		
	}

	public static boolean esOperador(String tipo){
		return tipo.equals("+") || tipo.equals("-") || tipo.equals("*") || 
				tipo.equals("/") || tipo.equals("^");
	}
	
	public static boolean esComparacion(String tipo){
		return tipo.equals("<") || tipo.equals("<=") || tipo.equals(">=") || 
				tipo.equals(">") || tipo.equals("==") || tipo.equals("<>");
	}

	public static boolean esOperacion(String linea) {
            Pila<Object> pilaParser = new Pila<Object>();
            Scanner scanner = new Scanner(linea);
            String token = "";
            String tokenaux = "";
            String accion = "";
            ArrayList<String> reduccion;
            boolean aceptado = false;
            boolean error = false;
            
            pilaParser.apilar(0);
            
            token = scanner.sgteToken();
                
            while(!aceptado && !error)  {  
                
                if(esID(token)){
                    tokenaux = "id";
                }else if(esReal(token) || esEntero(token)){
                    tokenaux = "num";
                }else if(esOperador(token)){
                    tokenaux = "ope";
                }else{
                    tokenaux = token;
                }
                
                accion = LR0.acccion((Integer)pilaParser.obtenerCima(), tokenaux);
                
                if(accion.startsWith("d")){
                    
                    pilaParser.apilar(tokenaux);
                    
                    pilaParser.apilar(Integer.parseInt(accion.substring(1)));
                    
                    token = scanner.sgteToken();
                    
                }else if(accion.startsWith("r")){
                    
                    String nuevoEstado = "";
                    String regla = "";
                    
                    pilaParser.desapilar();
                    
                    reduccion = LR0.reduccion(Integer.parseInt(accion.substring(1)));
                    
                    for (int i = 0; i < reduccion.size(); i++) {
                        
                        if(!pilaParser.obtenerCima().equals(reduccion.get(i))){
                            i--;
                        }
                        pilaParser.desapilar();
                    }
                    
                    regla = LR0.reglasProduccion(Integer.parseInt(String.valueOf(accion.charAt(1))));
                    
                    nuevoEstado = LR0.goTO((Integer)pilaParser.obtenerCima(),regla);
                    
                    
                    if(nuevoEstado.equals(""))
                        error = true;
                    
                    pilaParser.apilar(regla);
                    
                    pilaParser.apilar(Integer.parseInt(nuevoEstado));
                    
                }else if(accion.equals("Aceptar")){
                    aceptado = true;
                }else{
                    error = true;
                }
            } 
            
            if(aceptado)
                return true;
            else
                return false;
	}
        
    public static boolean esEntrada(String linea){
        
        int inicio = 0, fin = 5,error = -1, actual = inicio;
		Scanner scan = new Scanner(linea);
		String token = "";
	            
		while(actual != error &&  token != "$"){
			
	        token = scan.sgteToken();
	
	        switch(actual){
	            
	            case 0:
	                    if (token.equals("leer")){
	                            actual = 1;
	                    } else {
	                            actual = error;
	                    }
	                    break;
	            case 1:
	                    if (token.equals("(")){
	                            actual = 2;
	                    } else {
	                            actual = error;
	                    }
	                    break;
	            case 2:
	                    if (esID(token)){
	                            actual = 3;
	                    } else {
	                            actual = error;
	                    }
	                    break;
	            case 3:
	                    if (token.equals(")")){
	                            actual = 4;
	                    } else {
	                            actual = error;
	                    }
	                    break;
	            case 4:
	                    if (token.equals("$")){
	                            actual = fin;
	                    } else {
	                            actual = error;
	                    }
	                    break;
				}
                        
			}
			
			if(actual == fin){
				return true;
			} else {
				return false;
			}
        }
        public static boolean esAsignacionDeCadena(String linea){
		int inicio = 0, fin = 4,error = -1, actual = inicio;
		Scanner scan = new Scanner(linea);
		String token = "";
		while(actual != error &&  token != "$"){
			token = scan.sgteToken();
			switch(actual){
			case 0:
				if (esID(token)){
					actual = 1;
				} else {
					actual = error;
				}
				break;
			case 1:
				if (token.compareTo("=") == 0){
					actual = 2;
				} else {
					actual = error;
				}
				break;
			case 2:
				if (esConstanteCaracter(token)){
					actual = 3;
				} else {
					actual = error;
				}
				break;
			case 3:
				if (token.compareTo("$") == 0){
					actual = fin;
				} else {
					actual = error;
				}
				break;
			}
		}
		
		if(actual == fin){
			return true;
		} else {
			return false;
		}
		
        }
        public static boolean esConstanteCaracter(String tipo){
		return expresionRegular(tipo, "(\"(\\p{Print})+\"|\'(\\p{Print})\')") 
				|| tipo.equals("nulo") && !esBoleano(tipo);
        }
        
      public static boolean esSalida(String linea){
            
        int inicio = 0, fin = 6,error = -1, actual = inicio;
		Scanner scan = new Scanner(linea);
		String token = "";
                
		while(actual != error &&  token != "$"){
			
            token = scan.sgteToken();

            switch(actual){
                
                case 0:
                        if (token.equals("escribir")){
                                actual = 1;
                        } else {
                                actual = error;
                        }
                        break;
                case 1:
                        if (token.equals("(")){
                                actual = 2;
                        } else {
                                actual = error;
                        }
                        break;
                case 2:
                        if (esID(token) || esConstanteCaracter(token)){
                                actual = 4;
                        } else {
                                actual = error;
                        }
                        break;
                case 4:
                        if (token.equals(",")){
                                actual = 2;
                        } else if(token.equals(")")){
                                actual = 5;
                        }else {
                                actual = error;
                        }
                        break;
                case 5:
                        if (token.equals("$")){
                            actual = fin;
                        } else {
                            actual = error;
                        }
			}
                        
		}
		
		if(actual == fin){
			return true;
		} else {
			return false;
		}
    }
        
}
