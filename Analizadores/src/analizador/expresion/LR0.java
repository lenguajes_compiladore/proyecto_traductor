package analizador.expresion;

import java.util.ArrayList;

public class  LR0 {
    
	public static String acccion(int  cabezaPila, String token){
		int tokenInt;
		String[][] accion =    {{"d2","","","","","",""}, 
                                        {"","","","","","","Aceptar"},
					{"","d3","","","","",""},
					{"d14","","d7","","","d15",""},
                                        {"","","","r4","r4","","r1"},
					{"","","","","d9","",""},
                                        {"r3","r3","r3","r3","r3","r3","r3"},
                                        {"d14","","d7","","","d15",""},
                                        {"d14","","d7","","","d15",""},
                                        {"r6","r6","r6","r6","r6","r6","r6"},
                                        {"r4","r4","r4","d11","r4","r4","r4"},
                                        {"r5","r5","r5","r5","r5","r5","r5"},
                                        {"r2","r2","r2","r2","d9","r2","r2"},
                                        {"r4","r4","r4","r4","r4","r4","r4"},
                                        {"r7","r7","r7","r7","r7","r7","r7"},
                                        {"r8","r8","r8","r8","r8","r8","r8"}};
                
                switch(token){
		case "id": tokenInt = 0;
			break;
		case "=": tokenInt = 1;
			break;
		case "(": tokenInt = 2;
			break;
		case ")": tokenInt = 3;
			break;
		case "ope": tokenInt = 4;
			break;
		case "num": tokenInt = 5;
			break;
		case "$": tokenInt = 6;
			break;
		
		default: return "Error";
		}
		
		return accion[cabezaPila][tokenInt];
	}
        
    public static String goTO(int cabezaPila, String token){
        
        int tokenInt;
        String[][] goTo =    {{"1","","","",""}, 
                              {"","","","",""},
                              {"","","","",""},
                              {"","4","5","","6"},
                              {"","","","",""},
                              {"","","","8",""},
                              {"","","","",""},
                              {"","10","5","","6"},
                              {"","13","12","","6"},
                              {"","","","",""},
                              {"","","","",""},
                              {"","","","",""},
                              {"","","","8",""},
                              {"","","","",""},
                              {"","","","",""},
                              {"","","","",""}};
        
        switch (token){
            
            case "A": tokenInt = 0;
                      break;
            case "B": tokenInt = 1;
                      break;
            case "C": tokenInt = 2;
                      break;
            case "D": tokenInt = 3;
                      break;
            case "E": tokenInt = 4;
                      break;
            default:  tokenInt = -1;
                      break;
        }
        
        return goTo[cabezaPila][tokenInt];
    }
    
    public static String reglasProduccion(int regla){
        
        switch(regla){
            
            case 1: return "A";
            
            case 2: return "B";
            
            case 3: return "B";
            
            case 4: return "C";
            
            case 5: return "C";
            
            case 6: return "D";
            
            case 7: return "E";
            
            case 8: return "E";
            
            default: return "Error";
        }
    }
    
    public static ArrayList<String> reduccion(int regla){
        
        ArrayList<String> reglaProduccion = new ArrayList<>();
        
        switch(regla){
            
            case 1: reglaProduccion.add("B");
                    reglaProduccion.add("=");
                    reglaProduccion.add("id");
                    break;
                    
            case 2: reglaProduccion.add("C");
                    reglaProduccion.add("D");
                    reglaProduccion.add("C");
                    break;      
              
            case 3: reglaProduccion.add("E");
                    break;      
              
            case 4: reglaProduccion.add("B");
                    break;       
              
            case 5: reglaProduccion.add(")");
                    reglaProduccion.add("B");
                    reglaProduccion.add("(");
                    break;       
              
            case 6: reglaProduccion.add("ope");
                    break;     
              
            case 7: reglaProduccion.add("id");
                    break;     
              
            case 8: reglaProduccion.add("num");
                    break;              
            
        }
        
        return reglaProduccion;
    }
}
