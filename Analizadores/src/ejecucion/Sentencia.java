package ejecucion;

public class Sentencia {
	private String linea;
	private String tipo;
	private int numLinea;
	public Sentencia(String linea,int numLinea) {
		this.linea = linea;
		tipo = "";
		this.numLinea = numLinea;
	}

	public int getNumLinea() {
		return numLinea;
	}

	public String getLinea() {
		return linea;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
