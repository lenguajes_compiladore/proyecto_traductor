package ejecucion;

public class Variable implements Comparable<Variable>{
	private String id;
	private String tipo;
	private Object valor;
	
	public Variable(String id,String tipo, Object valor){
		this.id = id;
		this.tipo = tipo;
		this.valor = valor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int compareTo(Variable otraVariable) {
		return id.compareTo(otraVariable.id);
	}
	
	
}
