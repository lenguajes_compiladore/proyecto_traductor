package ejecucion;

public class VariableArreglo {
	private String tipo;
	private String id;
	private int base;
	private int dim1;
	private int dim2;
	private int varTam;

	public VariableArreglo(String tipo, String id, int base, int dim1, int dim2, int varTam) {
		this.tipo = tipo;
		this.id = id;
		this.base = base;
		this.dim1 = dim1;
		this.dim2 = dim2;
		this.varTam = varTam;
	}
	
	public int mapeo(int i, int j){
		return base + j * varTam * dim2 + i * varTam;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getDim1() {
		return dim1;
	}

	public void setDim1(int dim1) {
		this.dim1 = dim1;
	}

	public int getDim2() {
		return dim2;
	}

	public void setDim2(int dim2) {
		this.dim2 = dim2;
	}
}
