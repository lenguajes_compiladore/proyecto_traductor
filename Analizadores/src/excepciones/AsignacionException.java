package excepciones;

public class AsignacionException extends LenguajeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AsignacionException(){
		super ("El tipo de dato asignado no es correcto");
	}
}
