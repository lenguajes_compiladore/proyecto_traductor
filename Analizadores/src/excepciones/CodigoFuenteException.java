package excepciones;

public class CodigoFuenteException extends LenguajeException {

	private static final long serialVersionUID = 1L;
	private ErrorLinea error;
	public CodigoFuenteException () {
		super("El codigo no pertenece al lenguaje");
	}
	public CodigoFuenteException (String mensaje,ErrorLinea error) {
		super(mensaje);
		this.error = error;
	}
	public String getMessage() {
		return super.getMessage() + "\n" + error.toString() ;
	}
}
