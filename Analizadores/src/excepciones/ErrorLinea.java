package excepciones;

public class ErrorLinea {
	public static final String DOBLE_ASIGNACION = "Dos variables no pueden tener el mismo identificador";
	public static final String TIPOS_NO_COINCIDEN = "Las tipos no coinciden";
	public static final String VARIABLE_NO_DECLARADA = "La variable no ha sido declarada";

	private String error;
	private int numLinea;
	public ErrorLinea(String error,int linea) {
		this.error = error;
		this.numLinea = linea;
	}
	
	public String toString(){
		return "[Tipo: " + error + ",Linea: " + numLinea + "]";
	}
}
