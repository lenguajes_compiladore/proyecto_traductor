package excepciones;

public class LenguajeException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public LenguajeException (){
		super("Ha ocurrido un error de compilacion");
	}
	public LenguajeException (String mensaje) {
		super(mensaje);
	}
}
