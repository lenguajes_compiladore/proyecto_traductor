package excepciones;

public class VariableNoDeclaradaException extends LenguajeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VariableNoDeclaradaException(){
		super("La variable introducida no ha sido declarada");
	}
}
