package excepciones;

public class VariableRepetidaException extends LenguajeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VariableRepetidaException(){
		super("La variable ya ha sido instanciada");
	}
}
