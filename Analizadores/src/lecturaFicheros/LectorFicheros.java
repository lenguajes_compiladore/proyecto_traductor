package lecturaFicheros;
import java.io.*;
import java.util.ArrayList;

import ejecucion.Sentencia;
import util.Busquedas;

public class LectorFicheros {
	public static ArrayList<Sentencia> leerFicheroFuente (String ruta) throws IOException{
		
		ArrayList<Sentencia> codigoFuente = new ArrayList<Sentencia>() ;
		String linea = "";
		BufferedReader buffer;
		int contador = 1;
		buffer = new BufferedReader (new FileReader(ruta));
		while(linea != null) {
			linea = buffer.readLine();
			if(linea != null ) {
				linea = Busquedas.quitarTabulacion(linea);
				if(!linea.isEmpty()){
					codigoFuente.add(new Sentencia(linea,contador));
				}
				contador++;
			}
		}
		buffer.close();
		
		return codigoFuente;
	}
}


