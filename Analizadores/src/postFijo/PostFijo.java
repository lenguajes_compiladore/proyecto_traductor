package postFijo;

import java.util.ArrayList;
import java.util.Stack;
import analizador.*;
import analizador.expresion.ExpresionRegular;
import ejecucion.Sentencia;
import ejecucion.Variable;
import excepciones.ErrorLinea;
import util.Busquedas;
import util.pila.Pila;

public class PostFijo {

	public static String convertirPostFijo(String inFijo) {

		Pila<String> miPila = new Pila<String>();
		Scanner scan = new Scanner(inFijo);
		String postFijo = "";
		String token = "";
		while (!token.equals("$")) {
			token = scan.sgteToken();

			if (ExpresionRegular.esEntero(token) || ExpresionRegular.esReal(token) || ExpresionRegular.esID(token)) {
				postFijo += token + " ";
			} else if (Simbolo.esSimbolo(token)) {
				if (miPila.vacio()) {
					miPila.apilar(token);
				} else if (token.equals(")")) {
					while (!miPila.obtenerCima().equals("(")) {
						postFijo += miPila.desapilar() + " ";
					}
					miPila.desapilar();
				} else {
					boolean mayor = false;
					while (!mayor) {
						if (miPila.vacio() || Simbolo.pesoEnLaExpresionPostFija(token) >= Simbolo
								.pesoEnLaPilaPostFija(miPila.obtenerCima())) {
							miPila.apilar(token);
							mayor = true;
						} else {
							postFijo += miPila.desapilar() + " ";
						}
					}
				}
			}
		}

		while (!miPila.vacio()) {
			if (miPila.obtenerCima().equals("(")) {
				postFijo += miPila.desapilar() + " ";
			} else {
				postFijo += miPila.desapilar() + " ";
			}

		}
		return postFijo;
	}

	public static int obtenerResultadoInt(ArrayList<Variable> listaVariables,
			Stack<ErrorLinea> errores,Sentencia linea, String postFijo) {

		Pila<Integer> miPila = new Pila<Integer>();
		int resultado = 0, oper1, oper2, posVar;
		Scanner scan = new Scanner(postFijo);
		String token = "";
		while (!token.equals("$")) {
			posVar = 0;
			token = scan.sgteToken();
			if (!Simbolo.esSimbolo(token) && !token.equals("$")) {
				if (ExpresionRegular.esID(token)) {
					posVar = Busquedas.busqueda(listaVariables, token);
					if (posVar != -1) {
						if (listaVariables.get(posVar).getTipo().equals("entero")) {
							miPila.apilar((Integer) listaVariables.get(posVar).getValor());
						} else {
							 errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN + 
									 " (*->entero)",linea.getNumLinea()));
						}
					} else {
						errores.push(new ErrorLinea(ErrorLinea.VARIABLE_NO_DECLARADA, 
								linea.getNumLinea()));
					}
				} else {
					if (ExpresionRegular.esEntero(token)) {
						miPila.apilar(Integer.parseInt(token));
					} else {
						 errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN + 
								 " (*->entero)",linea.getNumLinea()));
					}
				}
			} else {
				switch (token) {
				case "+":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}

					miPila.apilar(oper1 + oper2);
					break;
				case "-":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(oper1 - oper2);
					break;
				case "*":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(oper1 * oper2);
					break;
				case "/":
					;
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(oper1 / oper2);
					break;
				case "^":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar((int) Math.pow(oper1, oper2));
					break;
				}
			}
		}

		resultado = miPila.obtenerCima();
		return resultado;
	}

	public static double obtenerResultadoDouble(ArrayList<Variable> listaVariables,
			Stack<ErrorLinea> errores,Sentencia linea, String postFijo) {
		Pila<Double> miPila = new Pila<Double>();
		double resultado = 0, oper1, oper2;
		int posVar;
		Scanner scan = new Scanner(postFijo);
		String token = "";
		while (!token.equals("$")) {
			posVar = 0;
			token = scan.sgteToken();
			if (!Simbolo.esSimbolo(token) && !token.equals("$")) {
				if (ExpresionRegular.esID(token)) {
					posVar = Busquedas.busqueda(listaVariables, token);
					if (posVar != -1) {
						if (listaVariables.get(posVar).getTipo().equals("real")) {
							miPila.apilar((Double) listaVariables.get(posVar).getValor());
						} else {
							errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN + 
									" (*->real)",linea.getNumLinea()));
						}
					} else {
						errores.push(new ErrorLinea(ErrorLinea.VARIABLE_NO_DECLARADA,
								linea.getNumLinea()));
					}
				} else {
					if (ExpresionRegular.esReal(token)) {
						miPila.apilar(Double.parseDouble(token));
					} else {
						errores.push(new ErrorLinea(ErrorLinea.TIPOS_NO_COINCIDEN + 
								" (*->real)",linea.getNumLinea()));
					}
				}
			} else {
				switch (token) {
				case "+":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(oper1 + oper2);
					break;
				case "-":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(oper1 - oper2);
					break;
				case "*":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(oper1 * oper2);
					break;
				case "/":
					;
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(oper1 / oper2);
					break;
				case "^":
					oper2 = miPila.desapilar();
					if (miPila.vacio()) {
						oper1 = 0;
					} else {
						oper1 = miPila.desapilar();
					}
					miPila.apilar(Math.pow(oper1, oper2));
					break;
				}
			}
		}

		resultado = miPila.obtenerCima();
		return resultado;
	}
}
