package postFijo;
public class Simbolo {
	public static boolean esSimbolo(String caracter){
		boolean esSimbolo = caracter.equals("+") || caracter.equals("-") || caracter.equals("*") ||
				caracter.equals("/") || caracter.equals("(")  || caracter.equals(")") ||
				caracter.equals("^");

	return esSimbolo;
	}

	public static int pesoEnLaExpresionPostFija(String simbolo){
		int peso = -1;
		
		switch(simbolo){
		case "+": case "-": peso = 1;
		break;
		case "*": case "/": peso = 2;
		break;
		case "^": peso = 4;
		break;
		case "(": peso = 5;
		break;
		}
		
		return peso;
	}

	public static int pesoEnLaPilaPostFija(String simbolo){
	
		int peso = -1;
		
		switch(simbolo){
		case "+": case "-": peso = 1;
		break;
		case "*": case "/": peso = 2;
		break;
		case "^": peso = 3;
		break;
		case "(": peso = 0;
		break;
		}
		return peso;
	}
}
