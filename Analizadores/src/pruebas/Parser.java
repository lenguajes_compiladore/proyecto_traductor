package pruebas;

import analizador.Scanner;

public class Parser {
	public static boolean declaracionVariable(String candidata){
		boolean pertenece = false;
		int fin = 3, actual = 0;
		Scanner scan = new Scanner(candidata);
		String sgteToken;
		do{
			sgteToken = scan.sgteToken();
			switch(actual){
				case 0: 
					if(sgteToken.equals("int")||sgteToken.equals("short") ||sgteToken.equals("long")||
					   sgteToken.equals("float")||sgteToken.equals("double")||sgteToken.equals("char")
					   ||sgteToken.equals("byte")||sgteToken.equals("bool")){
						actual = 1;
					}else{
						actual = -1;
					}
				break;
				case 1:
					if(esID(sgteToken)){
						actual = 2;
					}else{
						actual = -1;
					}
				break;
				case 2:
					if(sgteToken.equals(",")){
						actual = 1;
					}else if(sgteToken.equals(";")){
						actual = fin;
					}else{
						actual = -1;
					}
				break;
				case 3:
					if(sgteToken.equals("$")){
						actual = 3;
					}else{
						actual = -1;
					}
				break;
				case -1:
				break;
			}
		}while(!sgteToken.equals("$"));
		
		if(actual == fin){
			pertenece = true;
		}
		
		return pertenece;
		
	}
	
	public static boolean declaracionFuncion(String candidata){
		boolean esFuncion = false;
		int actual = 0;
		Scanner scan = new Scanner(candidata);
		String sgteToken;
	
		do{
			sgteToken = scan.sgteToken();
			switch(actual){
				case 0:
					if(esTipoFuncion(sgteToken)){
						actual = 1;
					}else{
						actual = -1;
					}
				break;
				case 1:
					if(esID(sgteToken)){
						actual = 2;
					}else{
						actual = -1;
					}
				break;
				case 2:
					if(sgteToken.equals("(")){
						actual = 3;
					}else{
						actual = -1;
					}
				break;
				case 3:
					if(sgteToken.equals(")")){
						actual = 4;
					}else if(esTipo(sgteToken)){
						actual = 5;
					}else{
						actual = -1;
					}
				break;
				case 4:
					if(sgteToken.equals("$")){
						actual = 8;
					}else{
						actual = -1;
					}
				break;
				case 5:
					if(esID(sgteToken)){
						actual = 6;
					}else{
						actual = -1;
					}
				break;
				case 6:
					if(sgteToken.equals(",")){
						actual = 7;
					}else if(sgteToken.equals(")")){
						actual = 4;
					}else{
						actual = -1;
					}
				break;
				case 7:
					if(esTipoFuncion(sgteToken)){
						actual = 5;
					}else{
						actual = -1;
					}
				break;
			}
		}while(!sgteToken.equals("$"));
		
		System.out.println(actual);
		if(actual == 8){
			esFuncion = true;
		}
		return esFuncion;
		
	}
	
	public static boolean esTipo(String cadena){
		return cadena.equals("int")||cadena.equals("short") ||cadena.equals("long")||
				cadena.equals("float")||cadena.equals("double")||cadena.equals("char")||
				cadena.equals("byte")||cadena.equals("bool");
	}
	public static boolean esTipoFuncion(String cadena){
		return cadena.equals("int")||cadena.equals("short") ||cadena.equals("long")||
				cadena.equals("float")||cadena.equals("double")||cadena.equals("char")||
				cadena.equals("byte")||cadena.equals("bool") || cadena.equals("void");
	}
	
	public static boolean esID(String cadena){
		boolean esID = false;
		int  fin = 1, actual = 0;
		char caracterEn;
		for (int i = 0; i < cadena.length(); i++) {
			caracterEn = cadena.charAt(i);
			switch(actual){
				case 0:
					if(Character.isAlphabetic(caracterEn)){
						actual = 1;
					}else{
						actual = -1;
					}
				break;
				case 1:
					if(Character.isAlphabetic(caracterEn)||Character.isDigit(caracterEn)|| caracterEn == '_'){
						actual = 1;
					}else{
						actual = -1;
					}
				break;
				case -1:
				break;
			}
		}
		
		if(actual == fin){
			esID = true;
		}
		
		return esID;
	}
}
