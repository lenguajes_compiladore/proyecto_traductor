package pruebas.automataPila;
import util.pila.Pila;;

public class AutomataPila {
	public static boolean PDA_1(String candidata){
		boolean pertenece = false;
		int inicio = 0, fin = 5, actual = 0;
		Pila<Character> miPila1 = new Pila<Character>();
		Pila<Character> miPila2 = new Pila<Character>();
		
		for (int i = 0; i < candidata.length(); i++) {
			if(actual == inicio){
				if(candidata.charAt(i) == 'a' && miPila1.vacio() && miPila2.vacio()){
					actual = 1;
					miPila1.apilar('a');
				}
			}else if(actual == 1){
				if(candidata.charAt(i) == 'a'  && !miPila1.vacio() && miPila2.vacio()){
					actual = 1;
					miPila1.apilar('a');
				}else if(candidata.charAt(i) == 'b' && !miPila1.vacio() && miPila2.vacio()){
					actual = 2;
					miPila2.apilar('b');
				}
			}else if(actual == 2){
				if(candidata.charAt(i) == 'b'  && !miPila1.vacio() && !miPila2.vacio()){
					actual = 2;
					miPila2.apilar('b');
				}else if(candidata.charAt(i) == 'c'  && !miPila1.vacio() && !miPila2.vacio()){
					actual = 3;
					miPila1.desapilar();
				}else{
					actual = -1;
				}
			}else if(actual == 3){
				if(candidata.charAt(i) == 'c'  && !miPila1.vacio() && !miPila2.vacio()){
					actual = 3;
					miPila1.desapilar();
				}else if(candidata.charAt(i) == 'd' && miPila1.vacio() && !miPila2.vacio()){
					actual = 4;
					miPila2.desapilar();
				}
				else{
					actual = -1;
				}
			}else if(actual == 4){
				if(candidata.charAt(i) == 'd' && miPila1.vacio()  && !miPila2.vacio()){
					actual = 4;
					miPila2.desapilar();
				}else{
					actual = -1;
				}
			}else{
				actual = -1;
			}
		}
		
		if(actual == 4 && miPila1.vacio() && miPila2.vacio()){
			actual = fin;
		}
		
		if(actual == fin){
			pertenece = true;
		}
		
		return pertenece;
	}
	
	public static boolean PDA_2(String candidata){
		boolean pertenece = false;
		char caracterEn;
		int inicio = 0, fin = 3, actual = 0, error = -1;
		Pila<Character> miPila = new Pila<Character>();
		
		for (int i = 0; i < candidata.length(); i++) {
			caracterEn = candidata.charAt(i);
			if(actual == inicio){
				if(caracterEn == 'a' && miPila.vacio()){
					actual = 1;
					miPila.apilar(caracterEn);
				}
			}else if(actual == 1){
				if(caracterEn == 'a' && miPila.obtenerCima() == 'a'){
					actual = 1;
					miPila.apilar(caracterEn);
				}else if (caracterEn == 'b' && miPila.obtenerCima() == 'a'){
					actual = 2;
					miPila.desapilar();
				}else{
					actual = error;
				}
			}else if(actual == 2){
				if (caracterEn == 'b' && miPila.obtenerCima() == 'a'){
					actual = 2;
					miPila.desapilar();
				}else{
					actual = error;
				}
			}
		}
		
		if(actual == 2 && !miPila.vacio()){
			actual = fin;
		}
		
		if(actual == fin) {
			pertenece = true;
		}
		
		return pertenece;
	}
	
	public static void main(String[] args) {
		String cadena = "aaaabbb";
		System.out.println(AutomataPila.PDA_2(cadena));
	}
}