package pruebas.automataPila.precedenciaSimple;
import util.pila.*;

public class PrecedenciaSimple {
	public static boolean parser (String candidata){
		boolean esAceptada = false;
		String pivote;
		char relacion,caracterEn;
		int i = 0;
		Pila<Character> miPila = new Pila<Character>();
		miPila.apilar('#');
		while(!esAceptada){
			
			pivote = "";
			caracterEn = candidata.charAt(i);
			relacion = relacion(miPila.obtenerCima(),caracterEn);
			
			if ( relacion == '<' || relacion == '=' ){
				miPila.apilar(relacion);
				miPila.apilar(caracterEn);
				i++;
			} else if ( relacion == '>') {
				do {
					pivote += miPila.desapilar();
				} while (miPila.obtenerCima() != '<');
				
				pivote += miPila.desapilar();
				
				if (pivote.equals("S<") && miPila.obtenerCima() == '#' && caracterEn == '$'){
					esAceptada = true;
				} else {
					miPila.apilar(relacion(miPila.obtenerCima(),obtenerA(pivote)));
					miPila.apilar(obtenerA(pivote));
				}
			} else {
				break;
			}
		}
		
		return esAceptada;
	}
	
	private static char obtenerA(String pivote){
		if (pivote.equals("c=X=a<")){
			return 'S';
		} else if (pivote.equals("B<") || pivote.equals("S<")){
			return 'X';
		} else if (pivote.equals("B=b<")|| pivote.equals("b<")){
			return 'B';
		} else {
			return 'n';
		}
	}
	
	private static char relacion(char cabezaPila,char token){
		
		int tokenInt,cabezaInt;
		char [][] matrizPrecedencia = {{'n','n','n','<','n','>','<','>'},
											  {'n','n','n','=','n','=','<','>'},
											  {'n','n','n','<','=','>','<','>'},
											  {'<','=','<','<','<','n','<','>'},
											  {'n','n','=','<','<','>','<','>'},
											  {'>','=','>','n','>','>','<','>'},
											  {'<','<','<','<','<','<','<','>'},
											  {'>','>','>','>','>','>','>','>'}};
		
		switch(token){
		case 'S': tokenInt = 0;
			break;
		case 'X': tokenInt = 1;
			break;
		case 'B': tokenInt = 2;
			break;
		case 'a': tokenInt = 3;
			break;
		case 'b': tokenInt = 4;
			break;
		case 'c': tokenInt = 5;
			break;
		case '#': tokenInt = 6;
			break;
		case '$': tokenInt = 7;
			break;
		
		default: tokenInt = -1;
			break;
		}
		
		switch(cabezaPila){
		case 'S': cabezaInt = 0;
			break;
		case 'X': cabezaInt = 1;
			break;
		case 'B': cabezaInt = 2;
			break;
		case 'a': cabezaInt = 3;
			break;
		case 'b': cabezaInt = 4;
			break;
		case 'c': cabezaInt = 5;
			break;
		case '#': cabezaInt = 6;
			break;
		case '$': cabezaInt = 7;
			break;
		default: cabezaInt = -1;
			break;
		}
		
		return (tokenInt != -1 && cabezaInt != -1)
				?matrizPrecedencia[cabezaInt][tokenInt]:'n';
	}
	
	
	public static void main(String[] args) {
		String cadena = "abc$";
		
		if (PrecedenciaSimple.parser(cadena)){
			System.out.println("La cadena '" + cadena + "' es aceptada");
		} else {
			System.out.println("Error!");
		}
		 
		
	}
}
