package util;

import java.util.*;
import ejecucion.Variable;

public class Busquedas {
	public static int busqueda(ArrayList<Variable> listaVariables,String id){
		boolean encontrado = false;
		int pos = -1, i = 0;
		
		Iterator<Variable> listaIterador = listaVariables.iterator();
		Variable variable;
		while (listaIterador.hasNext() && !encontrado) {
			variable = listaIterador.next();
			if(variable.compareTo(new Variable(id,"","")) == 0){
				encontrado = true;
				pos = i;
			}
			i++;
		}
		
		return pos;
	}
	
	
	
	public static String quitarTabulacion(String cadenaTabulada){
		return cadenaTabulada.replace('\t',' ');
	}
}
