package util.pila;
public class Pila <T>{
	
	private NodoDoble<T> inicio;
	private int tamanio;
	
	public Pila(){
		inicio = null;
		tamanio = 0;
	}
	
	public int tamanio(){
		return tamanio;
	}

	public void apilar(T dato){
		
		NodoDoble<T> nuevo = new NodoDoble<T>(dato);
		if(vacio()){
			inicio = nuevo;
		}else{
			NodoDoble<T> p = inicio;
			while(p.getSiguiente() != null){
				p = p.getSiguiente();
			}
			nuevo.setAnterior(p);
			p.setSiguiente(nuevo);
			p = nuevo;
		}
		tamanio++;
	}
	
	public T desapilar(){
		T nodoDesapilado = null;
		if(!vacio()){
			if(inicio.getSiguiente() == null){
				nodoDesapilado = inicio.getDato();
				inicio = null;
			}else{
				NodoDoble <T> p = inicio;
				while(p.getSiguiente() != null){
					p = p.getSiguiente();
				}
				nodoDesapilado = p.getDato();
				p = p.getAnterior();
				p.setSiguiente(null);
			}
		}
		tamanio--;
		return nodoDesapilado;
	}
	
	public void imprimir(){
		if(!vacio()){
			NodoDoble<T> p = inicio;
			while(p.getSiguiente() != null){
				p = p.getSiguiente();
			}
			
			while(p != null){
				System.out.println(p.getDato());
				p = p.getAnterior();
			}
		}
	}
	
	public boolean vacio(){
		return inicio == null;
	}
	
	public void eliminarPila(){
		int n = tamanio;
		for (int i = 0; i < n; i++) {
			desapilar();
		}
		
		
	}
	public T obtenerCima(){
		NodoDoble<T> p = inicio;
		while(p.getSiguiente() != null){
			p = p.getSiguiente();
		}
		T cima = p.getDato();
		return cima;
		
	}
}
